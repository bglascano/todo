const express = require("express")
const mongoose = require("mongoose")
const path = require('path')

const app = express()
app.use(express.json())
const PORT = process.env.PORT || 5000;

const db = "mongodb://localhost:27017/todoAppLascano"

mongoose.connect(process.env.MONGODB || db, ({useNewUrlParser: true, useUnifiedTopology: true}))
.then(console.log("Connected to MongoDB"))
.catch(err=>console.log(err))

const todoSchema = new mongoose.Schema({
	title: String,
	complete:{
		type: Boolean,
		default: false
	}
})

const Todo = mongoose.model('todo', todoSchema)

app.get("/todos",(req,res)=>{
	Todo.find().then(todo=>res.json(todo))
})

app.post("/todos", (req,res)=>{
	const newTodo = new Todo({
		title:req.body.title
	})
	newTodo.save().then(todo=>res.json(todo))
})

app.delete("/todos/:id", (req,res)=>{
	Todo.findByIdAndDelete(req.params.id)
	.then(()=>res.json({remove:true}))
})

if(process.env.NODE.ENV){
	app.use(express.static('./client/build'))
}


app.listen(PORT, ()=>{
	console.log("Server is running at port 5000")
});
